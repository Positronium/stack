# Stack

## Description

A simple game, using a state machine design pattern.

The goal is to stack moving bricks perfectly by clicking space in the right time. Failing to stack the brick either cuts off it's part, or ends the game. 

The starting point is Menu State, with options to Play, view High Score or Exit. Proceeding with Play changes active state to Game. Failing to stack a brick causes the active state to be Lose State.

## Mechanics

- Starting the game spawns one unmoving brick and one moving above it. Pressing space stacks them, or makes them fall, depending on the position. 
- Camera follows the active brick starting with a certain threshold, and when the brick falls.
- Score is counted and varies depending on perfect or partial stack. 
- The brick color is a gradient created from colors randomly chosen from a list.
