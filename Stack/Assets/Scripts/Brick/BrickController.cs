using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Stack.UI;
using Stack.Score;

public class BrickController : MonoBehaviour
{
    public delegate void OnGameOver();

    // Brick Management
    [SerializeField] private Transform brickParent;
    [SerializeField] private List<Brick> brickList;
    [SerializeField] private Brick brickPrefab;

    private Brick prevBrick;
    private Brick currentBrick;

    // Brick Posisions
    [SerializeField] private float threshold;
    private float positionDifference;
    private float leftSideDifference { get {return positionDifference  + currentBrick.leftPosition.x - prevBrick.leftPosition.x; } }
    private float rightSideDifference { get {return positionDifference + currentBrick.rightPosition.x - prevBrick.rightPosition.x; } }

    // Lost Game and Time Before LoseView
    private bool gameLost = false;
    [SerializeField] private float secondsBeforeLose;
    private UnityAction endGame;

    [Header("Camera")]
    [SerializeField] private CameraMovement cameraMovement;

    [Header("Color")]
    [SerializeField] private ColorPicker colorPicker;

    [Header("Sound")]
    [SerializeField] private SoundController soundController;

    public void InitializeController(UnityAction endListener)
    {
        StartNewGame();
        endGame = endListener;
    }

    public void DestroyController()
    {
        endGame = null;
    }
    public void UpdateController()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StackBricks();
        }


        cameraMovement.UpdateCamera(currentBrick.transform.position.y, gameLost);
    }

    private void StartNewGame()
    {
        gameLost = false;
        cameraMovement.ResetCamera();
        ScoreController.ResetScore();
        colorPicker.InitializeColors();

        currentBrick = Instantiate(brickPrefab, brickParent);
        currentBrick.StopMoving();
        currentBrick.transform.position = new Vector3(0f, -4.5f, 0f);
        currentBrick.gameObject.SetActive(true);
        brickList.Add(currentBrick);
        SpawnNewBrick();

        SetBrickColor(prevBrick, colorPicker.GetStartColor());
        SetBrickColor(currentBrick, colorPicker.GetCurrentColor());
    }

    private void EndGame()
    {
        StartCoroutine(WaitBeforeEnding());
    }

    public void ResetBricks()
    {
        if (brickList.Count > 0)
        {
            for (int i = brickList.Count - 1; i >= 0; i--)
            {
                Destroy(brickList[i].gameObject);
                brickList.Remove(brickList[i]);
            }
        }
        
    }

    private IEnumerator WaitBeforeEnding()
    {
        yield return new WaitForSeconds(secondsBeforeLose);

        endGame.Invoke();
    }

    private void StackBricks()
    {
        positionDifference = currentBrick.transform.position.x - prevBrick.transform.position.x;
        currentBrick.StopMoving();

        if (positionDifference >= -threshold & positionDifference <= threshold)
        {
            soundController.PlayPerfectStack();
            ScoreController.AddPerfectPoints();

            SnapToPosition();
            SpawnNewBrick();
            return;
        }
        else if (positionDifference < 0)
        {
            if (positionDifference <= prevBrick.leftPosition.x - currentBrick.rightPosition.x)
            {
                soundController.PlayLostGame();
                currentBrick.FallDown();
                gameLost = true;
                EndGame();
            }
            else
            {
                soundController.PlayCutBrick();
                ScoreController.AddBasicPoints();
                SpawnCutPart(prevBrick.leftPosition.x + prevBrick.transform.position.x, leftSideDifference, 0);
                currentBrick.CutLeftSide(currentBrick.leftPosition.x - leftSideDifference);
                SpawnNewBrick();
            }
        }
        else if (positionDifference > 0)
        {
            if (positionDifference >= prevBrick.rightPosition.x - currentBrick.leftPosition.x)
            {
                soundController.PlayLostGame();
                currentBrick.FallDown();
                gameLost = true;
                EndGame();
            }
            else
            {
                soundController.PlayCutBrick();
                ScoreController.AddBasicPoints();
                SpawnCutPart(prevBrick.rightPosition.x + prevBrick.transform.position.x, 0, rightSideDifference);
                currentBrick.CutRightSide(currentBrick.rightPosition.x - rightSideDifference);
                SpawnNewBrick();
            }
        }
    }
    
    private void SpawnNewBrick()
    {
        prevBrick = currentBrick;

        Vector3 newPosition = new Vector3(0 , prevBrick.transform.position.y + 1, 0);
        currentBrick = Instantiate(brickPrefab, newPosition, prevBrick.transform.rotation, brickParent);
        brickList.Add(currentBrick);

        currentBrick.SetPosition(prevBrick.leftPosition.x, prevBrick.rightPosition.x);
        SetBrickColor(currentBrick, colorPicker.GetCurrentColor());
        colorPicker.NextStep();

        currentBrick.brickMovement.SetSpeed(prevBrick.brickMovement.GetSpeed());
        currentBrick.gameObject.SetActive(true);
    }

    private void SpawnCutPart(float spawnPosition, float leftEdge, float rightEdge)
    {
        Brick leftover = Instantiate(brickPrefab);
        leftover.StopMoving();
        leftover.transform.position = new Vector3(spawnPosition, currentBrick.transform.position.y, currentBrick.transform.position.z);
        leftover.SetPosition(leftEdge, rightEdge);
        SetBrickColor(leftover, currentBrick.GetColor());
        leftover.gameObject.SetActive(true);
        leftover.FallDown();
        Destroy(leftover.gameObject, 3f);
    }
    private void SnapToPosition()
    {
        currentBrick.transform.position = new Vector3(prevBrick.transform.position.x, currentBrick.transform.position.y, currentBrick.transform.position.z);
    }
    private void SetBrickColor(Brick brick, Color color)
    {
        brick.UpdateColor(color);
    }
}
