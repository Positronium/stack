using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorPicker
{
    // #d60000, #bf6a00, #9d9900, #77bd4f, #4cdaa1, #00d5c5, #00cce1, #00c1f0, #1096d9, #3f6ab6, #543c88, #550051
    [SerializeField] private List<Color> availableColors;
    [SerializeField] private List<Color> currentColors;

    [SerializeField] private Color startColor;
    [SerializeField] private Color targetColor;
    [SerializeField] private Color currentColor;

    [SerializeField] private int step;
    [SerializeField] private int currentStep;

    public void InitializeColors()
    {
        currentColors.AddRange(availableColors);
        PickColor(out startColor);
        PickColor(out targetColor);
        currentStep = 1;
        currentColor = GetCurrentColor();
    }

    public void PickColor(out Color color)
    {
        if (currentColors.Count == 0)
            currentColors.AddRange(availableColors);
        color = currentColors[Random.Range(0, currentColors.Count)];
        currentColors.Remove(color);
    }

    public Color GetCurrentColor()
    {
        currentColor = Color.Lerp(currentColor, targetColor, (float)currentStep / step);
        return currentColor;
    }

    public Color GetStartColor()
    {
        return startColor;
    }

    public void NextStep()
    {
        currentStep++;
        if (currentStep > step)
        {
            currentStep = 1;
            startColor = targetColor;
            currentColor = GetCurrentColor();
            PickColor(out targetColor);
        }
    }
}
