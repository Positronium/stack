using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    [Header("Line Attributes")]
    [SerializeField] private LineRenderer brickRenderer;
    public Vector3 leftPosition;
    public Vector3 rightPosition;


    [Header("Movement Attributes")]
    public BrickMovement brickMovement;
    [SerializeField] private bool stopMoving = false;

    [SerializeField] private Rigidbody2D rb;


    public void Start()
    {
        SetPosition();
    }

    public void FixedUpdate()
    {
        if (stopMoving)
            return;
        brickMovement?.UpdateMovingPosition(transform);
    }

    public void UpdateColor(Color color)
    {
        brickRenderer.startColor = color;
        brickRenderer.endColor = color;
    }

    public void StopMoving()
    {
        stopMoving = true;
    }
    public void StartMoving()
    {
        stopMoving = false;
    }

    public void FallDown()
    {
        rb.isKinematic = false;
    }

    public void CutLeftSide(float x)
    {
        leftPosition = new Vector3(x, leftPosition.y, leftPosition.z);
        SetPosition();
    }    

    public void CutRightSide(float x)
    {
        rightPosition = new Vector3(x, rightPosition.y, rightPosition.z);
        SetPosition();

    }

    public void SetPosition(float leftX, float rightX)
    {
        leftPosition = new Vector3(leftX, leftPosition.y, leftPosition.z);
        rightPosition = new Vector3(rightX, rightPosition.y, rightPosition.z);
        SetPosition();
    }
    public void SetPosition()
    {
        brickRenderer.positionCount = 2;
        brickRenderer.SetPosition(0, leftPosition);
        brickRenderer.SetPosition(1, rightPosition);
    }

    public Color GetColor()
    {
        return brickRenderer.endColor;
    }

}
