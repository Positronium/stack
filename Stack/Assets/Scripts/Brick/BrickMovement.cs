using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BrickMovement
{
    [SerializeField] private float speed;
    [SerializeField] private float speedIncrease;
    [SerializeField] private float range;

    public void UpdateMovingPosition(Transform brickTransform)
    {
        var newX = Mathf.Lerp(-range, range, Mathf.InverseLerp(-1f, 1f, Mathf.Sin(Time.time * speed)));
        brickTransform.position = new Vector3(newX, brickTransform.position.y, brickTransform.position.z);
    }


    public void SetSpeed(float currentSpeed)
    {
        speed = currentSpeed + speedIncrease;
    }
    public float GetSpeed()
    {
        return speed;
    }
}
