using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stack.UI;
using Stack.Score;

namespace Stack.Core
{
    public class StateMachine : MonoBehaviour
    {
        private BaseState currentlyActiveState;

        private MenuState menuState;
        private GameState gameState;
        private LoseState loseState;

        [SerializeField] private MenuView menuView;
        [SerializeField] private ScorePopup scorePopup;
        [SerializeField] private GameView gameView;
        [SerializeField] private PauseView pauseView;
        [SerializeField] private LoseView loseView;

  
        [SerializeField] private BrickController brickController;

        private void Start()
        {
            InjectReferences();
            ChangeState(menuState); 

        }

        private void Update()
        {
            currentlyActiveState?.UpdateState();

        }

        private void OnDestroy()
        {
            currentlyActiveState?.DestroyState();
        }

        private void InjectReferences()
        {
            menuState = new MenuState(()=> ChangeState(gameState), menuView, scorePopup);
            gameState = new GameState(()=> ChangeState(loseState), brickController, gameView, pauseView);
            loseState = new LoseState(brickController, loseView, gameView, ()=> ChangeState(gameState), ()=> ChangeState(menuState));
        }





        private void ChangeState(BaseState state)
        {
            currentlyActiveState?.DestroyState();
            currentlyActiveState = state;
            currentlyActiveState?.InitializeState();
        }
    }
}
