using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stack.Core
{
    public class BaseState
    {
        
        public virtual void InitializeState()
        {

        }

        public virtual void UpdateState()
        {

        }
        public virtual void DestroyState()
        {

        }
    } 
}
