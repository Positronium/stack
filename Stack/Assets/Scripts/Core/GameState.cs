using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Stack.UI;
using Stack.Score;

namespace Stack.Core
{
    public class GameState : BaseState
    {

        private UnityAction changeStateToLose;
        private BrickController brickController;
        private GameView gameView;
        private PauseView pauseView;

        public GameState(UnityAction changeStateToLose, BrickController brickController,  GameView gameView, PauseView pauseView)
        {
            this.changeStateToLose = changeStateToLose;
            this.brickController = brickController;
            this.gameView = gameView;
            this.pauseView = pauseView;
        }
        public override void InitializeState()
        {
            base.InitializeState();
            pauseView.HideView();
            gameView.ShowView();
            gameView.UpdateScore(ScoreController.GetScore());
            

            brickController.InitializeController(changeStateToLose);
        }
         
        public override void UpdateState()
        {
            base.UpdateState();
            gameView.UpdateScore(ScoreController.GetScore());
            brickController.UpdateController();
        }

        public override void DestroyState()
        {
            gameView.HideView();
            brickController.DestroyController();
            base.DestroyState();
        }
    } 
}
