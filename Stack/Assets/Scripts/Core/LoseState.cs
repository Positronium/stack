using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Stack.UI;
using Stack.Score;

namespace Stack.Core
{
    public class LoseState : BaseState
    {
        private BrickController brickController;
        private LoseView loseView;
        private GameView gameView;
        private UnityAction changeStateToGame;
        private UnityAction changeStateToMenu;

        public LoseState(BrickController brickController, LoseView loseView, GameView gameView, UnityAction changeStateToGame, UnityAction changeStateToMenu)
        {
            this.brickController = brickController;
            this.loseView = loseView;
            this.gameView = gameView;
            this.changeStateToGame = changeStateToGame;
            this.changeStateToMenu = changeStateToMenu;
        }
        public override void InitializeState()
        {
            base.InitializeState();
            loseView.InitializeView(changeStateToGame, changeStateToMenu);
            loseView.DisplayScore(ScoreController.GetScore(), (ScoreController.highScoreList.Count > 0) ? ScoreController.highScoreList[0].entryScore : 0);
            loseView.ShowView();
        }

        public override void UpdateState()
        {
            base.UpdateState();
        }

        public override void DestroyState()
        {
            ScoreController.AddToHighScore(loseView.GetName(), ScoreController.GetScore());
            brickController.ResetBricks();
            loseView.HideView();
            loseView.DestroyView(changeStateToGame, changeStateToMenu);
            base.DestroyState();
        }
    } 
}
