using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Stack.UI;
using Stack.Score;

namespace Stack.Core
{
    public class MenuState : BaseState
    {
        private UnityAction changeStateToGame;
        private MenuView menuView;
        private ScorePopup scorePopup;


        public MenuState(UnityAction changeStateToGame, MenuView menuView, ScorePopup scorePopup)
        {
            this.changeStateToGame = changeStateToGame;
            this.menuView = menuView;
            this.scorePopup = scorePopup;
        }

        public override void InitializeState()
        {
            base.InitializeState();
            scorePopup.InitializeView();
            scorePopup.HideView();
            menuView.InitializeView(changeStateToGame, scorePopup.ShowView);
            menuView.ShowView();
        }

        public override void UpdateState()
        {
            base.UpdateState();
        }

        public override void DestroyState()
        {
            menuView.HideView();
            menuView.DestroyView(changeStateToGame, scorePopup.ShowView);
            scorePopup.DestroyView();
            base.DestroyState();
        }
    } 
}
