using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameraMovement
{
    [SerializeField] private Camera cam;
    [SerializeField] private float speed;
    [SerializeField] private float heightModifier;

    public void ResetCamera()
    {
        cam.transform.position = new Vector3(0, 0, -10);
    }
    public void UpdateCamera(float yPosition, bool isLost)
    {
        if (yPosition < heightModifier && !isLost)
            return;

        if (isLost)
        {
            yPosition = 0f;
        }
        float newY = Mathf.Lerp(cam.transform.position.y, yPosition, speed * Time.deltaTime);
        cam.transform.position = new Vector3(0f, newY, -10f);
    }

}
