using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundController
{
    [SerializeField] private AudioSource perfectStack;
    [SerializeField] private AudioSource cutBrick;
    [SerializeField] private AudioSource lostGame;

    public void PlayPerfectStack()
    {
        perfectStack.Play();
    }
    public void PlayCutBrick()
    {
        cutBrick.Play();
    }
    public void PlayLostGame()
    {
        lostGame.Play();
    }

}
