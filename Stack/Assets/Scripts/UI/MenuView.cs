using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Stack.UI
{
    public class MenuView : BasicView
    {
        [SerializeField] private Button playButton;
        [SerializeField] private Button scoreButton;
        [SerializeField] private Button exitButton;


        public void InitializeView(UnityAction playListener, UnityAction showScoreListener)
        {
            OnPlayButtonClicked_AddListener(playListener);
            OnScoreButtonClicked_AddListener(showScoreListener);
            OnExitButtonClicked_AddListener(Application.Quit);
        }

        public void DestroyView(UnityAction playListener, UnityAction showScoreListener)
        {
            OnPlayButtonClicked_RemoveListener(playListener);
            OnScoreButtonClicked_RemoveListener(showScoreListener);
            OnExitButtonClicked_RemoveListener(Application.Quit);
        }
        
        // ADDING REFERENCES
        private void OnPlayButtonClicked_AddListener(UnityAction listener)
        {
            playButton.onClick.AddListener(listener);
        }
        private void OnScoreButtonClicked_AddListener(UnityAction listener)
        {
            scoreButton.onClick.AddListener(listener);
        }
        private void OnExitButtonClicked_AddListener(UnityAction listener)
        {
            exitButton.onClick.AddListener(listener);
        }

        // REMOVING REFERENCES
        private void OnPlayButtonClicked_RemoveListener(UnityAction listener)
        {
            playButton.onClick.RemoveListener(listener);
        }
        private void OnScoreButtonClicked_RemoveListener(UnityAction listener)
        {
            scoreButton.onClick.RemoveListener(listener);
        }
        private void OnExitButtonClicked_RemoveListener(UnityAction listener)
        {
            exitButton.onClick.RemoveListener(listener);
        }
    } 
}
