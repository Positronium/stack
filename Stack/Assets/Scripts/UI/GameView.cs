using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Stack.UI
{
    public class GameView : BasicView
    {
        [SerializeField] private TextMeshProUGUI scoreText;

        public void UpdateScore(int score)
        {
            //scoreText.text = "SCORE: " + score.ToString();
            scoreText.text = string.Format("SCORE: {0:0000}", score);
        }
    } 
}
