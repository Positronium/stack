using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using Stack.Score;


namespace Stack.UI
{
    public class LoseView : BasicView
    {
        [SerializeField] private Button retryButton;
        [SerializeField] private Button menuButton;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI highScoreText;
        [SerializeField] private TMP_InputField nameInput;

        public void InitializeView(UnityAction retryListener, UnityAction menuListener)
        {
            OnRetryButtonClicked_AddListener(retryListener);
            OnMenuButtonClicked_AddListener(menuListener);
        }

        public void DestroyView(UnityAction retryListener, UnityAction menuListener)
        {
            OnRetryButtonClicked_RemoveListener(retryListener);
            OnMenuButtonClicked_RemoveListener(menuListener);
        }

        // ADDING REFERENCES
        private void OnRetryButtonClicked_AddListener(UnityAction listener)
        {
            retryButton.onClick.AddListener(listener);
        }

        private void OnMenuButtonClicked_AddListener(UnityAction listener)
        {
            menuButton.onClick.AddListener(listener);
        }

        // REMOVING REFERENCES

        private void OnRetryButtonClicked_RemoveListener(UnityAction listener)
        {
            retryButton.onClick.RemoveListener(listener);
        }

        private void OnMenuButtonClicked_RemoveListener(UnityAction listener)
        {
            menuButton.onClick.RemoveListener(listener);
        }


        public void DisplayScore(int score, int highScore)
        {
            scoreText.text = string.Format("SCORE: {0:0000}", score);
            highScoreText.text = string.Format("TOP SCORE: {0:0000}", highScore);
        }

        public string GetName()
        {
            if (nameInput.text == "")
                return "Unknown";
            return nameInput.text;
        }
    } 
}
