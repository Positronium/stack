using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Stack.Score;

namespace Stack.UI
{
    public class ScoreEntry : BasicView
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI scoreText;
        public void DisplayEntry(string name, int score)
        {
            nameText.text = name;
            scoreText.text = string.Format("{0.00000}", score);
        }

        public void HideEntry()
        {
            nameText.text = " ";
            scoreText.text = " ";
        }
    } 
}
