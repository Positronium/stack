using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stack.UI
{
    public class BasicView : MonoBehaviour
    {
        public void ShowView()
        {
            gameObject.SetActive(true);
        }

        public void HideView()
        {
            gameObject.SetActive(false);
        }
    } 
}
