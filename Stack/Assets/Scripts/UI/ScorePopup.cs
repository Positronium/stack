using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using Stack.Score;

namespace Stack.UI
{
    public class ScorePopup : BasicView
    {
        [SerializeField] private Button backButton;
        [SerializeField] private TextMeshProUGUI[] names;
        [SerializeField] private TextMeshProUGUI[] scores;

        public void SetScore()
        {
            for (int i = 0; i < Mathf.Min(ScoreController.highScoreList.Count, names.Length); i++)
            {
                names[i].text = ScoreController.highScoreList[i].entryName;
                scores[i].text = string.Format("{0:0000}", ScoreController.highScoreList[i].entryScore);
            }
        }

        public void InitializeView()
        {
            OnBackButtonClicked_AddListener(HideView);
            SetScore();
        }

        public void DestroyView()
        {
            OnBackButtonClicked_RemoveListener(HideView);
        }

        // ADDING REFERENCES

        private void OnBackButtonClicked_AddListener(UnityAction listener)
        {
            backButton.onClick.AddListener(listener);
        }

        // REMOVING REFERENCES

        private void OnBackButtonClicked_RemoveListener(UnityAction listener)
        {
            backButton.onClick.RemoveListener(listener);
        }
    } 
}
