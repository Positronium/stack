using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Stack.Score;

namespace Stack.Score
{
    public class ScoreController
    {
        private static int basicPoints = 5;
        private static int perfectPoints = 20;
        private static int score;
        public static string name = "Unknown";

        public static List<ScoreEntry> highScoreList = new List<ScoreEntry>();


        #region Score
        public static int GetScore()
        {
            return score;
        }

        public static void AddBasicPoints()
        {
           
            score += basicPoints;
        }
        public static void AddPerfectPoints()
        {
            score += perfectPoints;
        }

        public static void ResetScore()
        {
            score = 0;
        }
        #endregion

        #region High Score

        public static void AddToHighScore(string name, int score)
        {
            highScoreList.Add(new ScoreEntry { entryName = name, entryScore = score });
            SortList();
        }

        private static void SortList()
        {
            highScoreList = highScoreList.OrderByDescending(entry => entry.entryScore).ToList();
        }

        #endregion
    } 
}
